import xmltodict
import requests
import logging

LOGGER = logging.getLogger(__name__)

class ListMatching(object):
    """ListMatching API, XML body POST and response parse"""
    def __init__(self):
        self.match = False

    def list_match(self, pagetitle, list_type):
        """API call to listmatching algorithm. POSTS xml body"""
        pagetitle = pagetitle.encode('utf-8')
        url = "http_list_matching_api"
        headers = {'content-type': 'text/xml'}
        body = """<?xml version='1.0' encoding='UTF-8'?>
                <iaps-request action="list-matching">
                <directive>
                <method report="first_match_found" match="full"/>
                <results compress="no"/>
                </directive> 
                <source> 
                <list>{}</list>
                <data><![CDATA[{}]]></data> 
                </source>
                </iaps-request>""".format(list_type, pagetitle)
        response = requests.post(url, data=body, headers=headers)
        doc = xmltodict.parse(response.content)
        artist = album = track = ''
        try:
            type = doc['iaps-response']['matches']['match']['product-type']['#text']
            self.match = True
            if type == "Album":
                artist = doc['iaps-response']['matches']['match']['artist']['#text']
                album = doc['iaps-response']['matches']['match']['asset']['#text']
                track = ''
            else:
                artist = doc['iaps-response']['matches']['match']['artist']['#text']
                album = ''
                track = doc['iaps-response']['matches']['match']['asset']['#text']

            return artist, album, track
        except:
            return artist, album, track


