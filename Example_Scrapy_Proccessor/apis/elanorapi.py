import requests
import json
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import logging

LOGGER = logging.getLogger(__name__)

class ElanorAPI(object):
    """connection and interactions with Elanor API"""
    def __init__(self):
        self.api_post_url = '''http_api_url'''
        self.api_status_url = '''http_api_url'''

        self.api_status = self.check_api_status()
        if not self.api_status:
            raise StatusCodeError('Api appears to be down.')

    def json_str(self, obj_list):
        """Creates JSON body required for posting results to elanor"""
        json_str = '''{"links":[
          {"ID":"py_rss_feed",
           "OWNER_ID":1,
           "PROVIDER_OVERRIDE_ID":31,
           "ROBOT_NAME":"py_rss_feed",
           "DATE_DETECTED":"%s",
           "INFRINGING_URL":"%s",
           "ARTIST":"%s",
           "TRACK":"%s",
           "ALBUM":"%s",
           "REFERRER":"%s",
           "PAGE_TITLE":"%s"
           }]}''' % (obj_list[0].replace('"', '\\"').replace('\n', '\\n'),
                     obj_list[1].replace('"', '\\"').replace('\n', '\\n'),
                     obj_list[2].replace('"', '\\"').replace('\n', '\\n'),
                     obj_list[3].replace('"', '\\"').replace('\n', '\\n'),
                     obj_list[4].replace('"', '\\"').replace('\n', '\\n'),
                     obj_list[5].replace('"', '\\"').replace('\n', '\\n'),
                     obj_list[6].replace('"', '\\"').replace('\n', '\\n'))
        return json_str

    def check_api_status(self):
        """Check if API is up before making posts"""
        try:
            requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
            self.api_status_code = requests.get(self.api_status_url)
        except requests.exceptions.ConnectionError as err:
            LOGGER.error(err, exc_info = True)
            return False

        if '200' not in str(self.api_status_code):
            LOGGER.error('The API returned status code "{}". Aborting...'.format(self.api_status_code), exc_info = True)
            return False
        return True

    def inf_input(self, obj_list):
        """Post links to elanor"""
        print("Putting links in Elanor...")
        self.json = self.json_str(obj_list)
        try:
            requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
            self.api_put_status_code = requests.post(self.api_post_url, self.json)
            print('API Post JSON: "{}"'.format(self.json))
            print('API returned text: {}'.format(self.api_put_status_code.text))
            logging.matched += 1
        except requests.exceptions.ConnectionError as err:
            LOGGER.error('Error "{}" to call API. Parameter: "{}"'.format(err, self.json), exc_info = True)
            logging.SQL_error += 1
            logging.SQL_message.append(err)

        if '200' not in str(self.api_put_status_code):
            LOGGER.error('PUT call to the API returned status "{}"'.format(self.api_put_status_code.status_code), exc_info = True)
            raise StatusCodeError