import requests
import xmltodict
from requests.packages.urllib3.exceptions import InsecureRequestWarning
import logging

LOGGER = logging.getLogger(__name__)

class InfringingCheck(object):
    """Class to make a new instance of isInfringing check"""
    def __init__(self):
        self.inf_links = []
        self.infringing_status = ['1', '11', '12']

    def is_infringing(self, urls):
        """Method to check if links are infringing using isinfringing API"""
        LOGGER.debug("Checking links for infringment..")
        requests.packages.urllib3.disable_warnings(InsecureRequestWarning)
        sess = requests.session()
        for link in urls:
            try:
                is_infringing_response = sess.get(
                    "http_is_infringing_api" + link
                    )
            except ConnectionError as e:
                LOGGER.error("Connection error for Infringing check: " + str(e), exc_info = True)

            if "Error" in str(is_infringing_response.content):
                LOGGER.error('isInfringing error', exc_info = True)

            else:
                try:
                    is_infringing_response = xmltodict.parse(is_infringing_response.content)
                    status = is_infringing_response['iaps-response']['detail']
                    if status in self.infringing_status:
                        self.inf_links.append(link)
                        LOGGER.debug("Link added to list: {}".format(link))
                except TypeError as e:
                    LOGGER.error("xml conversion error:", exc_info = True)

        if not self.inf_links:
            LOGGER.debug("No infringing links found")
        sess.close()
        return self.inf_links

