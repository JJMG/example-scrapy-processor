import requests
import xmltodict

class Blockwords(object):
    """Creates a list of blockwords from an API"""
    def __init__(self):
        self.blockword_xml = requests.get('http_blockwords_api')
        self.blockword_list = []

    def blockwords_create(self):
        blockword_xml = xmltodict.parse(self.blockword_xml.content)
        for word in blockword_xml['syncServiceResponse']['items']['item']:
            self.blockword_list.append(word['title'])
