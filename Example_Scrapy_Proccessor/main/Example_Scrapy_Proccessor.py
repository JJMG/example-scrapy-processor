#!usr/bin/python
import requests
import json
import datetime
import logging
from multiprocessing.dummy import Pool as ThreadPool
from aws.sqs import SQS
from aws.s3 import S3
from apis.listmatching import ListMatching
from apis.elanorapi import ElanorAPI
from apis.infringingcheck import InfringingCheck
from apis.blockwords import Blockwords

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)
BLOCKWORDS = Blockwords()
BLOCKWORDS.blockwords_create()
SQS_CLIENT = SQS()

class Item:
    """"JSON representation of referrer. Calls all processing function and determines response"""    
    def __init__(self, s3_results):
        self.lists = ["Type_20", "general_iapu", "Confidential Titles Crawl"]
        self.pagetitle = self.parsed_item['pagetitle']
        self. referrer = self.parsed_item['referrer']
        self.links = self.parsed_item['links']
        self.infringing_links = []
        self.receipt_handle = item_result['ReceiptHandle']
        self.artist = ''
        self.title = ''
        self.album = ''

    def try_blockwords(self):
        """Checks if pagetitle contains a blockword"""
        for word in self.pagetitle:
            if word in BLOCKWORDS.blockword_list:
                return True
        return False

    def try_list_matching(self):
        """Checks the pagetitle against the listmatching algorithm for matches"""
        matching = ListMatching()
        for list_type in self.lists:
             self.artist, self.album, self.track = matching.list_match(self.pagetitle,list_type)
             if self.artist: break

    def try_infringing(self):
        """Checks if the link is of infringing type"""
        is_infringing_check = InfringingCheck()
        self.infringing_links = is_infringing_check.is_infringing(self.links)



def processor(item_result):
    """Main logic handler"""
    try:
        s3_object = S3(item_result['key'])
        s3_results = s3_object.get_file()
        current_item = Item(s3_result)
        block_test = current_item.try_blockwords()
        if block_test:
            status = "blockword"
            #ENTER INTO NOT-MATCHED TABLE
            return
        current_item.try_list_matching()
        if not current_item.artist:
            status = "no match"
            #ENTER INTO NOT-MATCHED TABLE
            return
        current_item.try_infringing()
        if not current_item.infringing_links:
            status = "no links"
            #ENTER INTO NOT-MATCHED TABLE
            return

        elanor_api = ElanorAPI()
        date = datetime.datetime.now()
        for infringing_link in current_item.infringing_links:
            obj_list = (date, infringing_link,
                        current_item.artist, current_item.title,
                        current_item.album, current_item.referrer,
                        current_item.pagetitle)
            elanor_api.inf_input(obj_list)

        LOGGER.info("ADDING TO ELANOR")
    except Exception as e:
        raise e

    finally:
        SQS_CLIENT.sqs_message_delete(item_result['Receipthandle'])#@todo review


if __name__ == "__main__":
    """Program start"""
    LOGGER.info('Start')
    SQS_PENDING = True

    while SQS_PENDING:
        try:
            SQS_PENDING = SQS_CLIENT.message_count()
            SQS_MESSAGES = SQS_CLIENT.retrieved_messages()
            POOL = ThreadPool(2)
            blockwords()
            POOL.map(processor, SQS_MESSAGES['Messages'])
            POOL.close()
            POOL.join()
        except Exception as err:
            LOGGER.error('Main application error', exc_info = True)

    LOGGER.info('Finished')