import boto3
import json
"""
Not yet implemented
"""
class S3(object):
    """Connection to s3 resources, returns json"""
    def __init__(self):

        self.s3_client = boto3.client('s3')
        self.s3_resource = boto3.resource('s3')
        #self.s3_bucket = self.s3_client.Bucket('bucket name here')

    def get_file(self, bucket, key_from_sqs):
        #s3_bucket = self.s3_client.Bucket('test_bucket')
        file = self.s3_client.get_object(Bucket = bucket, Key = key_from_sqs)
        json_data = file['Body'].read().decode('utf-8')
        json_data = json.loads(json_data)
        return json_data


