import boto3
import logging
from botocore.exceptions import ClientError

LOGGER = logging.getLogger(__name__)

class SQS(object):
    """SQS worknode"""
    def __init__(self):
        self.sqs_client = boto3.client('sqs', region_name='us-west-1')
        self.q_sqs = 'aws url here'

    def message_count(self):
        try:
            message_count = self.sqs_client.get_queue_attributes(QueueUrl=self.q_sqs,
                                                            AttributeNames=['ApproximateNumberOfMessages'])
            if '0' is message_count['Attributes']['ApproximateNumberOfMessages']:
                    LOGGER.info("Queue count is 0.")
                    return False
            else:
                return True
        except ClientError as e:
            LOGGER.error('SQS handling error', exc_info = True)

    def retrieved_messages(self):
        try:
            sqs_response = self.sqs_client.receive_message(QueueUrl = self.q_sqs,
                                                       MaxNumberOfMessages = 10,
                                                       MessageAttributeNames = ['All'])
            return sqs_response

        except ClientError as e:
            LOGGER.error('SQS handling error', exc_info = True)

    def sqs_message_delete(self, receipt_handle):
        """Delete the SQS message from queue"""
        LOGGER.debug("Deleting from queue")
        try:
            sqs_d = self.sqs_client.delete_message(QueueUrl=self.q_sqs,
                                            ReceiptHandle=receipt_handle)
            LOGGER.debug("Message removed from queue")
        except ClientError as err:
            LOGGER.error('SQS handling error', exc_info = True)


