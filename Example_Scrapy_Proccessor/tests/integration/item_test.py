import unittest
import moto
import boto3
import requests
import json
import logging
from main.Example_Scrapy_Proccessor import Item
from apis.blockwords import Blockwords
from apis.infringingcheck import InfringingCheck
from apis.listmatching import ListMatching

class ItemTest(unittest.TestCase):
    def test_try_blockwords(self):
        sqs_example_response = {'Messages': [{'MessageId': '10a2f8f8-bfc8-18f2-5338-419f4249acbc', 'Body': '{"links": ["https://www45.zippyshare.com/v/1KoqYGBc/file.html", "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"], "pagetitle": "test title test", "referrer": "https://leakcity.org/2018/09/classic-usher-my-way-1997/"}', 'ReceiptHandle': 'tbqxsldqyhiasomvasbtsveoiisgfpkuujsnutdixaqykqftxwazeilvncgmqaeijdaudpvenhzipplaypngcnreyalxsrzzcqzsantaaqjnwhaoyygsjuqtacnnxhnjatyvzpjfhsulqrntcauodxxumuvsewvaemrwfofiwmjrjawxtckvhjskh', 'MD5OfBody': '91906b734f991b855232d9cc2783a695', 'Attributes': {'SenderId': 'AIDAIT2UOQQY3AUEKVGXU', 'SentTimestamp': '1543240418593', 'ApproximateReceiveCount': '1', 'ApproximateFirstReceiveTimestamp': '1543240418641'}}], 'ResponseMetadata': {'RetryAttempts': 0, 'HTTPStatusCode': 200, 'RequestId': None, 'HTTPHeaders': {'x-amzn-requestid': 'W912M6NLQTBBOXO19VV05LLQ75H3RXD2V1G24IUHFYPD18U7N3EW', 'x-amz-crc32': '2238193047', 'server': 'amazon.com'}}}
        infringing_init = Item(sqs_example_response['Messages'][0])
        BLOCKWORDS = Blockwords()
        BLOCKWORDS.blockwords_create()
        blockwords_test = infringing_init.try_blockwords()
        self.assertEqual(False, blockwords_test)

    def test_try_list_matching(self):
        sqs_example_response = {'Messages': [{'MessageId': '10a2f8f8-bfc8-18f2-5338-419f4249acbc', 'Body': '{"links": ["https://www45.zippyshare.com/v/1KoqYGBc/file.html", "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"], "pagetitle": "EMINEM LUCKY YOU", "referrer": "https://leakcity.org/2018/09/classic-usher-my-way-1997/"}', 'ReceiptHandle': 'tbqxsldqyhiasomvasbtsveoiisgfpkuujsnutdixaqykqftxwazeilvncgmqaeijdaudpvenhzipplaypngcnreyalxsrzzcqzsantaaqjnwhaoyygsjuqtacnnxhnjatyvzpjfhsulqrntcauodxxumuvsewvaemrwfofiwmjrjawxtckvhjskh', 'MD5OfBody': '91906b734f991b855232d9cc2783a695', 'Attributes': {'SenderId': 'AIDAIT2UOQQY3AUEKVGXU', 'SentTimestamp': '1543240418593', 'ApproximateReceiveCount': '1', 'ApproximateFirstReceiveTimestamp': '1543240418641'}}], 'ResponseMetadata': {'RetryAttempts': 0, 'HTTPStatusCode': 200, 'RequestId': None, 'HTTPHeaders': {'x-amzn-requestid': 'W912M6NLQTBBOXO19VV05LLQ75H3RXD2V1G24IUHFYPD18U7N3EW', 'x-amz-crc32': '2238193047', 'server': 'amazon.com'}}}
        infringing_init = Item(sqs_example_response['Messages'][0])
        infringing_init.try_list_matching()
        self.assertEqual("EMINEM", infringing_init.artist)

    def test_try_infringing(self):
        sqs_example_response = {'Messages': [{'MessageId': '10a2f8f8-bfc8-18f2-5338-419f4249acbc', 'Body': '{"links": ["http://businessnewsstories.online/lx4quda23zmp.mp3", "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"], "pagetitle": "EMINEM LUCKY YOU", "referrer": "https://leakcity.org/2018/09/classic-usher-my-way-1997/"}', 'ReceiptHandle': 'tbqxsldqyhiasomvasbtsveoiisgfpkuujsnutdixaqykqftxwazeilvncgmqaeijdaudpvenhzipplaypngcnreyalxsrzzcqzsantaaqjnwhaoyygsjuqtacnnxhnjatyvzpjfhsulqrntcauodxxumuvsewvaemrwfofiwmjrjawxtckvhjskh', 'MD5OfBody': '91906b734f991b855232d9cc2783a695', 'Attributes': {'SenderId': 'AIDAIT2UOQQY3AUEKVGXU', 'SentTimestamp': '1543240418593', 'ApproximateReceiveCount': '1', 'ApproximateFirstReceiveTimestamp': '1543240418641'}}], 'ResponseMetadata': {'RetryAttempts': 0, 'HTTPStatusCode': 200, 'RequestId': None, 'HTTPHeaders': {'x-amzn-requestid': 'W912M6NLQTBBOXO19VV05LLQ75H3RXD2V1G24IUHFYPD18U7N3EW', 'x-amz-crc32': '2238193047', 'server': 'amazon.com'}}}
        infringing_init = Item(sqs_example_response['Messages'][0])
        infringing_init.try_infringing()
        self.assertTrue(infringing_init.infringing_links)