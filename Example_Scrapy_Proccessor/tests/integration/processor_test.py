import unittest
import moto
import boto3
import requests
import json
import logging
import main.Example_Scrapy_Proccessor as main
from aws.sqs import SQS

class ProcessorTest(unittest.TestCase):
    def test_processor(self):
        with moto.mock_sqs():
            SQS_CLIENT = SQS()
            queue = SQS_CLIENT.sqs_client.create_queue(QueueName='scrape-test')
            queue_url = SQS_CLIENT.sqs_client.get_queue_url(QueueName = 'scrape-test')
            SQS_CLIENT.q_sqs = queue_url['QueueUrl']
            json_data = {"pagetitle": "EMINEM LUCKY YOU",
                         "referrer": "https://leakcity.org/2018/09/classic-usher-my-way-1997.mp3",
                         "links": ["https://www45.zippyshare.com/v/1KoqYGBc/file.html",
                                   "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"]}
            sendMessage = SQS_CLIENT.sqs_client.send_message(QueueUrl = SQS_CLIENT.q_sqs,
                             MessageBody = json.dumps(json_data))
            messages = SQS_CLIENT.retrieved_messages()

            main.processor(messages['Messages'][0])


if __name__ == '__main__':

    unittest.main()
