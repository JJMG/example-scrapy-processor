import moto
import unittest
import json
from apis.listmatching import ListMatching

class ListMatchingTest(unittest.TestCase):
    def test_init(self):
        with moto.mock_sqs():
            MATCHING = ListMatching()
            self.assertEqual(False, MATCHING.match)

    def test_list_match(self):
        lists = ["Type_20", "general_iapu", "Confidential Titles Crawl"]
        title = "EMINEM LUCKY YOU"
        MATCHING = ListMatching()
        artist, album, track = MATCHING.list_match(title, lists[2])
        self.assertEqual(False, MATCHING.match)
        self.assertEqual('', artist)

        title = "EMINEM LUCKY YOU"
        artist, album, track = MATCHING.list_match(title, lists[1])
        self.assertEqual(True, MATCHING.match)
        self.assertEqual('EMINEM', artist)
