import unittest
import moto
import boto3
import requests
import json
import logging
from main.Example_Scrapy_Proccessor import Item

class ItemTest(unittest.TestCase):
    def test_init(self):
        sqs_example_response = {'Messages': [{'MessageId': '10a2f8f8-bfc8-18f2-5338-419f4249acbc', 'Body': '{"links": ["https://www45.zippyshare.com/v/1KoqYGBc/file.html", "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"], "pagetitle": "test title test", "referrer": "https://leakcity.org/2018/09/classic-usher-my-way-1997/"}', 'ReceiptHandle': 'tbqxsldqyhiasomvasbtsveoiisgfpkuujsnutdixaqykqftxwazeilvncgmqaeijdaudpvenhzipplaypngcnreyalxsrzzcqzsantaaqjnwhaoyygsjuqtacnnxhnjatyvzpjfhsulqrntcauodxxumuvsewvaemrwfofiwmjrjawxtckvhjskh', 'MD5OfBody': '91906b734f991b855232d9cc2783a695', 'Attributes': {'SenderId': 'AIDAIT2UOQQY3AUEKVGXU', 'SentTimestamp': '1543240418593', 'ApproximateReceiveCount': '1', 'ApproximateFirstReceiveTimestamp': '1543240418641'}}], 'ResponseMetadata': {'RetryAttempts': 0, 'HTTPStatusCode': 200, 'RequestId': None, 'HTTPHeaders': {'x-amzn-requestid': 'W912M6NLQTBBOXO19VV05LLQ75H3RXD2V1G24IUHFYPD18U7N3EW', 'x-amz-crc32': '2238193047', 'server': 'amazon.com'}}}
        infringing_init = Item(sqs_example_response['Messages'][0])
        self.assertEqual('test title test', infringing_init.pagetitle)
        self.assertEqual(["https://www45.zippyshare.com/v/1KoqYGBc/file.html", "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"], infringing_init.links)
        self.assertEqual('https://leakcity.org/2018/09/classic-usher-my-way-1997/', infringing_init.referrer)
        self.assertEqual('tbqxsldqyhiasomvasbtsveoiisgfpkuujsnutdixaqykqftxwazeilvncgmqaeijdaudpvenhzipplaypngcnreyalxsrzzcqzsantaaqjnwhaoyygsjuqtacnnxhnjatyvzpjfhsulqrntcauodxxumuvsewvaemrwfofiwmjrjawxtckvhjskh', infringing_init.receipt_handle)
        self.assertEqual(["Type_20", "general_iapu", "Confidential Titles Crawl"], infringing_init.lists)

if __name__ == '__main__':
    unittest.main()
