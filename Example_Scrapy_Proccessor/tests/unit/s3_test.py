import unittest
import moto
import boto3
import json
import os
from aws.s3 import S3


class Test_s3_test(unittest.TestCase):
    def test_s3(self):
        file = open("C:\\Users\\Julien\\Source\\repos\\Example_Scrapy_Proccessor\\Example_Scrapy_Proccessor\\tests\\unit\\leakcity.org.json", 'rb')
        with moto.mock_s3():
            s3_client = S3()
            #s3 = boto3.client('s3')
            #s3_client = S3()
            s3_client.s3_resource.create_bucket(Bucket = 'test_bucket')
            s3_client.s3_client.put_object(Bucket='test_bucket', Key='testkey.json', Body=file)
            file.close()
            file_from_s3 = s3_client.get_file('test_bucket', 'testkey.json')
            print(file_from_s3)


if __name__ == '__main__':
    unittest.main()
