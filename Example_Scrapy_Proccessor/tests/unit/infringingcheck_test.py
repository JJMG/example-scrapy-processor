import moto
import requests
import unittest
from apis.infringingcheck import InfringingCheck

class InfringingCheckTest(unittest.TestCase):
    def test_init(self):
        infringing_init = InfringingCheck()
        self.assertEqual([], infringing_init.inf_links)
        self.assertEqual(['1', '11', '12'], infringing_init.infringing_status)

    def testis_infringing(self):
        urls = ["https://leakcity.org/2018/09/classic-usher-my-way-1997/",
                "https://www45.zippyshare.com/v/1KoqYGBc/file.html.mp3",
                "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"]
        is_infringing = InfringingCheck()
        inf_links = is_infringing.is_infringing(urls)
        self.assertEqual(["https://www45.zippyshare.com/v/1KoqYGBc/file.html.mp3"], inf_links)
