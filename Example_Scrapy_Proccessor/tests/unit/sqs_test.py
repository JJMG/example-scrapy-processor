import moto
import requests
import unittest
import json
from aws.sqs import SQS

class SQSTest(unittest.TestCase):
    def test_init(self):
        with moto.mock_sqs():
            SQS_CLIENT = SQS()
            queue = SQS_CLIENT.sqs_client.create_queue(QueueName='scrape-test')
            queue_url = SQS_CLIENT.sqs_client.get_queue_url(QueueName='scrape-test')
            SQS_CLIENT.q_sqs = queue_url['QueueUrl']
            response = SQS_CLIENT.sqs_client.get_queue_attributes(QueueUrl = SQS_CLIENT.q_sqs,
                                                                  AttributeNames = ['All'])
            
    def test_message_count(self):
        with moto.mock_sqs():
            SQS_CLIENT = SQS()
            queue = SQS_CLIENT.sqs_client.create_queue(QueueName='scrape-test')
            queue_url = SQS_CLIENT.sqs_client.get_queue_url(QueueName = 'scrape-test')
            SQS_CLIENT.q_sqs = queue_url['QueueUrl']
            SQS_MESSAGES = SQS_CLIENT.message_count()
            self.assertEqual(False, SQS_MESSAGES)

    def test_received_messages(self):
        with moto.mock_sqs():
            SQS_CLIENT = SQS()
            queue = SQS_CLIENT.sqs_client.create_queue(QueueName='scrape-test')
            queue_url = SQS_CLIENT.sqs_client.get_queue_url(QueueName = 'scrape-test')
            SQS_CLIENT.q_sqs = queue_url['QueueUrl']
            json_data = {"pagetitle": "test title test",
                         "referrer": "https://leakcity.org/2018/09/classic-usher-my-way-1997/",
                         "links": ["https://www45.zippyshare.com/v/1KoqYGBc/file.html",
                                   "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"]}
            sendMessage = SQS_CLIENT.sqs_client.send_message(QueueUrl = SQS_CLIENT.q_sqs,
                             MessageBody = json.dumps(json_data))
            messages = SQS_CLIENT.retrieved_messages()
            print(messages)
            for item in messages['Messages']:
                parsed_item = json.loads(item['Body'])
                self.assertEqual('test title test', parsed_item['pagetitle'])
            
    def test_sqs_message_delete(self):

        with moto.mock_sqs():
            SQS_CLIENT = SQS()
            queue = SQS_CLIENT.sqs_client.create_queue(QueueName='scrape-test')
            queue_url = SQS_CLIENT.sqs_client.get_queue_url(QueueName = 'scrape-test')
            SQS_CLIENT.q_sqs = queue_url['QueueUrl']
            json_data = {"pagetitle": "test title test",
                         "referrer": "https://leakcity.org/2018/09/classic-usher-my-way-1997/",
                         "links": ["https://www45.zippyshare.com/v/1KoqYGBc/file.html",
                                   "https://www45.zippyshare.com/v/O6nPDz1Z/file.html"]}
            sendMessage = SQS_CLIENT.sqs_client.send_message(QueueUrl = SQS_CLIENT.q_sqs,
                             MessageBody = json.dumps(json_data))
            messages = SQS_CLIENT.retrieved_messages()
            SQS_CLIENT.sqs_message_delete(messages['Messages'][0]['ReceiptHandle'])
            #self.assertEquals('test title test', parsed_item['pagetitle'])
    
if __name__ == '__main__':
    unittest.main()
